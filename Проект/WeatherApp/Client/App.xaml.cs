﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Client
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        public ResourceDictionary ThemeDictionary
    {
        // You could probably get it via its name with some query logic as well.
        get { return Resources.MergedDictionaries[0]; }
    }

    public void ChangeTheme(Uri style, Uri style2, Uri color_Button, Uri background)
    {
        ThemeDictionary.MergedDictionaries.Clear();
        ThemeDictionary.MergedDictionaries.Add(new ResourceDictionary() { Source = style });
        ThemeDictionary.MergedDictionaries.Add(new ResourceDictionary() { Source = style2 });
        ThemeDictionary.MergedDictionaries.Add(new ResourceDictionary() { Source = color_Button });
        ThemeDictionary.MergedDictionaries.Add(new ResourceDictionary() { Source = background });
    }
    }

}
