﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Server;

namespace Client
{
    /// <summary>
    /// Логика взаимодействия для RegWindow.xaml
    /// </summary>
    public partial class RegWindow : Window
    {
        private readonly TcpService tcpService;
        private MainWindow MainW;
        public RegWindow(MainWindow MainW)
        {
            InitializeComponent();
            this.MainW = MainW;
            tcpService = new TcpService();
        }

        protected override void OnClosed(EventArgs e)
        {
            MainW.Back(true);
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            MainW.Back(false);
            this.Close();
        }

        private async void logButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MainWindow.inputNotNull(loginBox.Text);
                MainWindow.inputNotNull(passwordBox.Password);
                MainWindow.inputNotNull(passwordRepeatBox.Password);
                string login = loginBox.Text;
                string password = passwordBox.Password;

                if (passwordBox.Password != passwordRepeatBox.Password)
                {
                    throw new ArgumentException("Пароль не співпадає");
                }

                var requestUser = new Users
                {
                    Login = login,
                    Password = password,
                };


                string request = tcpService.SerializeAddUserRequest(requestUser);
                byte[] data = await tcpService.CodeStreamAsync(request);
                await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
                string response = await tcpService.DecodeStreamAsync(SingletoneObj.Stream);
                var responseArgs = response.Split(';');
                if (responseArgs.Length > 1 && responseArgs[0].Contains("0"))
                {
                    throw new ArgumentException(responseArgs[1]);
                }
                if (responseArgs.Contains("1"))
                {
                    MessageBox.Show("Реєстрація пройшла успішно");
                }
                MainW.Back(false);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
