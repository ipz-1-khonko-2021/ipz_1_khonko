﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Server;

namespace Client
{
    /// <summary>
    /// Логика взаимодействия для WeatherWindow.xaml
    /// </summary>
    public partial class WeatherWindow : Window
    {
        private bool active = false;
        private MainWindow MainW;
        private readonly TcpService tcpService;
        public WeatherWindow(MainWindow MainW)
        {
            InitializeComponent();
            this.MainW = MainW;
            tcpService = new TcpService();
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            MainW.Back(false);
            this.Close();
        }

        protected override void OnClosed(EventArgs e)
        {
            MainW.Back(true);
        }


        public async void Back(bool mode)
        {
            if (mode == true && active == false)
            {
                Application.Current.Shutdown();
            }
            else if (mode == true && active == true)
            {
                this.Show();
                active = false;
            }
            else if (mode == false)
            {
                active = true;
            }
        }
        private async void button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (comboBox.SelectedIndex == -1)
                {
                    throw new ArgumentException("Не вибране місто");
                }
                LocDate ld = new LocDate();
                ld.Location = comboBox.Text;

                inputNotNull(year.Text);
                inputNotNull(mounth.Text);
                inputNotNull(day.Text);

                if (int.Parse(year.Text) > 2020 || int.Parse(year.Text) < 2018)
                {
                    throw new ArgumentException("Не коректний рік");
                }
                if (int.Parse(mounth.Text) < 1 || int.Parse(mounth.Text) > 12)
                {
                    throw new ArgumentException("Не коректний місяць");
                }
                if (int.Parse(mounth.Text) < 10 )
                {
                    mounth.Text = "0" + mounth.Text;
                }
                if (int.Parse(day.Text) < 1 || int.Parse(day.Text) > 30)
                {
                    throw new ArgumentException("Не коректний день");
                }
                if (day.Text.Length < 2)
                {
                    day.Text = "0" + day.Text;
                }
                ld.Year = year.Text;
                ld.Mounth = mounth.Text;
                ld.Day = day.Text;

                string request = " ";
                request = tcpService.SerializeSearch(ld);
                byte[] data = await tcpService.CodeStreamAsync(request);
                await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
                string response = await tcpService.DecodeStreamAsync(SingletoneObj.Stream);
                WeatherDetails weather = tcpService.DeserializeSearch(response);

                if (weather == null)
                {
                    throw new ArgumentException("Немає даних");
                }
                else
                {
                    switch (weather.sky)
                    {
                        case 1:
                            image.Source = (ImageSource)(new ImageSourceConverter()).ConvertFrom("1.png");
                            sky.Text = "Сонячно";
                            break;
                        case 2:
                            image.Source = (ImageSource)(new ImageSourceConverter()).ConvertFrom("2.png");
                            sky.Text = "Пасмурно";
                            break;
                        case 3:
                            image.Source = (ImageSource)(new ImageSourceConverter()).ConvertFrom("3.png");
                            sky.Text = "Хмарно";
                            break;
                        case 4:
                            image.Source = (ImageSource)(new ImageSourceConverter()).ConvertFrom("4.png");
                            sky.Text = "Дощ";
                            break;
                        default:
                            break;
                    }
                    temp.Text = "Температура: " + weather.temperature.ToString();
                    tempMin.Text = "Історичний мінімум: " + weather.temperatureMin.ToString();
                    tempMax.Text = "Історичний максимум: " + weather.temperatureMax.ToString();
                    humidity.Text = "Вологість: " + weather.humidity.ToString() + "%";
                    wind.Text = "Вітер: " + weather.wind.ToString() + " км/год";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }

        public static void inputNotNull(string st)
        {
            if (st.Length <= 0)
            {
                throw new ArgumentException("Поля не можуть бути пусті");
            }
            if (!int.TryParse(st, out int j))
            {
                throw new ArgumentException("Не коректний формат дати");
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ThemePage tp = new ThemePage(this);
            tp.Show();
            this.Hide();
        }
    }
}
