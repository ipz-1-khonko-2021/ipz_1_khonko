﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;



namespace Client

{
    /// <summary>
    /// Interaction logic for ThemePage.xaml
    /// </summary>
    public partial class ThemePage : Window
    {
        private WeatherWindow WeatherW;
        private Color color;

      
        public ThemePage(WeatherWindow WeatherW)
        {
            InitializeComponent();
            this.WeatherW = WeatherW;
            
        }
        enum Color
        {
            Red, Yellow, Lime
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
            var app = (App)Application.Current;
            switch (color)
            {
                    case Color.Red:
                    app.ChangeTheme(new Uri("pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme.Light.xaml"),
                                    new Uri("pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme.Defaults.xaml"),
                                    new Uri("pack://application:,,,/MaterialDesignColors;component/Themes/Recommended/Primary/MaterialDesignColor.Red.xaml"),
                                    new Uri("pack://application:,,,/MaterialDesignColors;component/Themes/Recommended/Accent/MaterialDesignColor.Red.xaml"));
                    break;
                    case Color.Yellow:
                    app.ChangeTheme(new Uri("pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme.Light.xaml"),
                                    new Uri("pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme.Defaults.xaml"),
                                    new Uri("pack://application:,,,/MaterialDesignColors;component/Themes/Recommended/Primary/MaterialDesignColor.Yellow.xaml"),
                                    new Uri("pack://application:,,,/MaterialDesignColors;component/Themes/Recommended/Accent/MaterialDesignColor.Yellow.xaml"));
                    break; 
                    case Color.Lime:
                    app.ChangeTheme(new Uri("pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme.Light.xaml"),
                                    new Uri("pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme.Defaults.xaml"),
                                    new Uri("pack://application:,,,/MaterialDesignColors;component/Themes/Recommended/Primary/MaterialDesignColor.Lime.xaml"),
                                    new Uri("pack://application:,,,/MaterialDesignColors;component/Themes/Recommended/Accent/MaterialDesignColor.Lime.xaml"));
                    break;
            }
       

        }

        private void Red_Click(object sender, RoutedEventArgs e)
        {
            color = Color.Red;
        }
        private void Yellow_Click(object sender, RoutedEventArgs e)
        {
            color = Color.Yellow;
        }
        private void Lime_Click(object sender, RoutedEventArgs e)
        {
            color = Color.Lime;
        }
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            WeatherW.Back(false);
            this.Close();
            
        }
        protected override void OnClosed(EventArgs e)
        {
            WeatherW.Back(true);
        }

    }
}
