﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Server;
using System.Net;
using System.Net.Sockets;

namespace Client
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public static class SingletoneObj
    {
        public static Users User { get; set; }
        public static int Port { get; set; }
        public static string IP { get; set; }
        public static TcpClient Client { get; set; }
        public static NetworkStream Stream { get; set; }
    }

    public partial class MainWindow : Window
    {
        public readonly TcpService tcpService;
        public static string ip;
        public static int port;
        private bool active = false;
        public MainWindow() 
        {
            InitializeComponent();
            this.tcpService = new TcpService();
            SingletoneObj.Port = 4115;
            SingletoneObj.IP = "192.168.43.166";
            SingletoneObj.Client = new TcpClient(SingletoneObj.IP, SingletoneObj.Port);
            SingletoneObj.Stream = SingletoneObj.Client.GetStream();

        }

        private void regButton_Click(object sender, RoutedEventArgs e)
        {
            RegWindow rw = new RegWindow(this);
            rw.Show();
            this.Hide();
        }

        private void logButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string login = loginBox.Text;
                string password = passwordBox.Password;

                inputNotNull(login);
                inputNotNull(password);

                string request = tcpService.SerializeAuthorizeRequest(login, password);
                byte[] data = tcpService.CodeStream(request);
                SingletoneObj.Stream.Write(data, 0, data.Length);
                string response = tcpService.DecodeStream(SingletoneObj.Stream);
                Users user = tcpService.DeserializeAuthorizeResponse(response);
                if (user.Login == null || user.Password == null || !user.Login.Equals(login) || !user.Password.Equals(password))
                    throw new ArgumentException("Логін або пароль не вірні");
                WeatherWindow ww = new WeatherWindow(this);
                ww.Show();
                this.Hide();
                loginBox.Clear();
                passwordBox.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public async void Back(bool mode)
        {
            if (mode == true && active == false)
            {
                string request = "LogOut";
                byte[] data = await tcpService.CodeStreamAsync(request);
                await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
                Application.Current.Shutdown();
            }
            else if (mode == true && active == true)
            {
                this.Show();
                active = false;
            }
            else if (mode == false)
            {
                active = true;
            }
        }

        protected async override void OnClosed(EventArgs e)
        {
            this.Close();
            string request = "LogOut";
            byte[] data = await tcpService.CodeStreamAsync(request);
            await SingletoneObj.Stream.WriteAsync(data, 0, data.Length);
        }

        public static void inputNotNull(string st)
        {
            if (st.Length <= 0)
            {
                throw new ArgumentException("Поля не можуть бути пусті");
            }
        }
    }
}
