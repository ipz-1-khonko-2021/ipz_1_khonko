﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Server
{
    class DataService
    {
        private readonly DataBase _dbContext;
        public DataService(DataBase dbContext)
        {
            _dbContext = dbContext;
        }



        public List<Users> GetAllUsers()
        {
            return _dbContext.Users.ToList();
        }


        public Users GetUser(string login, string password)
        {
            return _dbContext.Users.Where(el => el.Login.Equals(login) && el.Password.Equals(password)).FirstOrDefault();
        }



        public void InsertUser(Users user)
        {
            var userInDb = _dbContext.Users.FirstOrDefault(el => el.Login.Equals(user.Login));
            if (userInDb != null)
            {
                throw new ArgumentException("Логін існує в базі даних");
            }
            var query = @"INSERT INTO Users (Login, Password) VALUES (@Login, @Password)";
            _dbContext.Database.ExecuteSqlCommand(query, new SqlParameter("@Login", user.Login), new SqlParameter("@Password", user.Password));
            var userId = _dbContext.Users.FirstOrDefault(el => el.Login.Equals(user.Login)).UserId;
        }

        public WeatherDetails Search(LocDate request)
        {
            WeatherDetails weather = _dbContext.Database.SqlQuery<WeatherDetails>("select * from WeatherDetails" +
                " where dateW = '" + request.Year + "-" + request.Mounth + "-" + request.Day + "' and location = '"+ request.Location +"'",
             new SqlParameter("@location", request.Location)).FirstOrDefault();
            return weather;
        }
    }
}
