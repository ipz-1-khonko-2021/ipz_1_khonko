﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Server
{
    class Program
    {
        static TcpListener listener;
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            using (DataBase context = new DataBase())
            {
                int port = 4115;
                string address = "0.0.0.0";
                try
                {
                    listener = new TcpListener(IPAddress.Parse(address), port);
                    listener.Start();
                    Console.WriteLine("Очікування підключення ...");
                    while (true)
                    {
                        TcpClient client = listener.AcceptTcpClient();
                        ClientObject clientObject = new ClientObject(client, context);
                        Thread clientThread = new Thread(new ThreadStart(clientObject.Process));
                        clientThread.Start();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    if (listener != null)
                    {
                        listener.Stop();
                    }
                }
            }
        }
    }
}
