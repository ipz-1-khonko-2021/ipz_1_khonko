﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Net.Sockets;

namespace Server
{
    public class SocketNode
    {
        public string Method { get; set; }
        public string Args { get; set; }
    }
    public class LocDate
    {
        public string Year { get; set; }
        public string Mounth { get; set; }
        public string Day { get; set; }
        public string Location { get; set; }
    }
    public class TcpService
    {
        private readonly DataService dataService;
        public TcpService(DataBase context = null)
        {
            if (context != null)
                dataService = new DataService(context);
        }


        public string DecodeAndProcessRequest(string request)
        {
            var socketNode = JsonSerializer.Deserialize<SocketNode>(request);
            TcpMethods tcpMethod;
            string response = "";
            if (!Enum.TryParse<TcpMethods>(socketNode.Method, out tcpMethod))
            {
                tcpMethod = TcpMethods.NONE;
            }

            switch (tcpMethod)
            {
                case TcpMethods.Authorize:
                    response = Authorize(socketNode);
                    break;
                case TcpMethods.AddUser:
                    response = AddUser(socketNode);
                    break;
                case TcpMethods.Search:
                    response = Search(socketNode);
                    break;
                default:
                    break;
            }

            return response;
        }

        public async Task<string> DecodeStreamAsync(NetworkStream stream)
        {
            byte[] data = new byte[64];
            StringBuilder builder = new StringBuilder();
            int bytes = 0;
            do
            {
                bytes = await stream.ReadAsync(data, 0, data.Length);
                builder.Append(Encoding.UTF8.GetString(data, 0, bytes));
            }
            while (stream.DataAvailable);
            return builder.ToString();
        }

        public string DecodeStream(NetworkStream stream)
        {
            byte[] data = new byte[64];
            StringBuilder builder = new StringBuilder();
            int bytes = 0;
            do
            {
                bytes = stream.Read(data, 0, data.Length);
                builder.Append(Encoding.UTF8.GetString(data, 0, bytes));
            }
            while (stream.DataAvailable);
            return builder.ToString();
        }

        public async Task<byte[]> CodeStreamAsync(string request)
        {
            return await Task.Run(() => CodeStream(request));
        }

        public byte[] CodeStream(string request)
        {
            return Encoding.UTF8.GetBytes(request);
        }

        public string SerializeAuthorizeRequest(string login, string password)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "Authorize",
                Args = JsonSerializer.Serialize<Users>(new Users
                {
                    Login = login,
                    Password = password
                })
            });
        }

        private string Authorize(SocketNode node)
        {
            string response = "";
            Users requestUser = JsonSerializer.Deserialize<Users>(node.Args);

            if (requestUser != null)
            {
                Users user = dataService.GetUser(requestUser.Login, requestUser.Password);
                if (user == null)
                {
                    user = new Users();
                }
                response = JsonSerializer.Serialize<Users>(user);
            }

            return response;
        }

        public Users DeserializeAuthorizeResponse(string response)
        {
            return JsonSerializer.Deserialize<Users>(response);
        }


        public string SerializeAddUserRequest(Users newUser)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "AddUser",
                Args = JsonSerializer.Serialize<Users>(newUser)
            });
        }

        private string AddUser(SocketNode socketNode)
        {
            try
            {
                Users user = JsonSerializer.Deserialize<Users>(socketNode.Args);
                dataService.InsertUser(user);
                return "1";
            }
            catch (Exception ex)
            {
                return "0;" + ex.Message;
            }
        }

        public string SerializeSearch(LocDate request)
        {
            return JsonSerializer.Serialize<SocketNode>(new SocketNode
            {
                Method = "Search",
                Args = JsonSerializer.Serialize<LocDate>(request)
            });
        }

        private string Search(SocketNode socketNode)
        {
            string response = "";
            LocDate request = JsonSerializer.Deserialize<LocDate>(socketNode.Args);
            WeatherDetails weather = dataService.Search(request);
            response = JsonSerializer.Serialize<WeatherDetails>(weather);
            return response;
        }

        public WeatherDetails DeserializeSearch(string response)
        {
            return JsonSerializer.Deserialize<WeatherDetails>(response);
        }
    }



    public enum TcpMethods
    {
        NONE,
        Authorize,
        AddUser,
        Search
    }
}
