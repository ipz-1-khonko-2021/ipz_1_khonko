namespace Server
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class WeatherDetails
    {
        public int WeatherDetailsId { get; set; }

        [StringLength(101)]
        public string location { get; set; }

        [Column(TypeName = "date")]
        public DateTime? dateW { get; set; }

        public int? sky { get; set; }

        public int? temperature { get; set; }

        public int? temperatureMin { get; set; }

        public int? temperatureMax { get; set; }

        public int? humidity { get; set; }

        public int? wind { get; set; }
    }
}
